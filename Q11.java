import java.util.Scanner;

public class Q11 {
    public static String digitToEnglish(char digit) {
        String digit_eng = "";
        switch (digit) {
            case '1':
                digit_eng = "one";
                break;

            case '2':
                digit_eng = "two";
                break;

            case '3':
                digit_eng = "three";
                break;

            case '4':
                digit_eng = "four";
                break;

            case '5':
                digit_eng = "five";
                break;

            case '6':
                digit_eng = "six";
                break;

            case '7':
                digit_eng = "seven";
                break;

            case '8':
                digit_eng = "eight";
                break;

            case '9':
                digit_eng = "nine";
                break;

            case '0':
                digit_eng = "";
                break;
        }
        return digit_eng;
    }

    public static String tensToEnglish(char digit) {
        String tens_digit_eng = "";
        switch (digit) {
            case '1':
                tens_digit_eng = "ten";
                break;

            case '2':
                tens_digit_eng = "twenty";
                break;

            case '3':
                tens_digit_eng = "thirty";
                break;

            case '4':
                tens_digit_eng = "fourty";
                break;

            case '5':
                tens_digit_eng = "fifty";
                break;

            case '6':
                tens_digit_eng = "sixty";
                break;

            case '7':
                tens_digit_eng = "seventy";
                break;

            case '8':
                tens_digit_eng = "eighty";
                break;

            case '9':
                tens_digit_eng = "ninety";
                break;

            case '0':
                tens_digit_eng = "";
                break;
        }
        return tens_digit_eng;
    }

    public static String betweenTenAndTwentyToEnglish(char digit) {
        String digits_eng = "";
        switch (digit) {
            case '1':
                digits_eng = "eleven";
                break;

            case '2':
                digits_eng = "twelve";
                break;

            case '3':
                digits_eng = "thirteen";
                break;

            case '4':
                digits_eng = "fourteen";
                break;

            case '5':
                digits_eng = "fifteen";
                break;

            case '6':
                digits_eng = "sixteen";
                break;

            case '7':
                digits_eng = "seventeen";
                break;

            case '8':
                digits_eng = "eighteen";
                break;

            case '9':
                digits_eng = "nineteen";
                break;
        }
        return digits_eng;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String N = scanner.next();
        int len = N.length();
        String number_in_english = "";

        for(int i=0; i<len; i++) {
            char digit = N.charAt(i);
            int position = len - i;

            String digit_eng = digitToEnglish(digit);
            String value = "";
            switch (position) {
                case 6:
                    value = "hundred";
                    break;

                case 5:
                    digit_eng = tensToEnglish(digit);
                    value = "";
                    if(digit == '1' && N.charAt(i+1) != '0') {
                        i++;
                        digit_eng = betweenTenAndTwentyToEnglish(N.charAt(i));
                        value = "tousand";
                    }
                    break;

                case 4:
                    value = "thousand";
                    break;

                case 3:
                    value = "hundred";
                    if (digit == '0') value = "";
                    break;

                case 2:
                    digit_eng = tensToEnglish(digit);
                    value = "";
                    if(digit == '1' && N.charAt(i+1) != '0') {
                        i++;
                        digit_eng = betweenTenAndTwentyToEnglish(N.charAt(i));
                    }
                    break;
                case 1:
                    value = "";
                    break;
            }
            String space_dig = "";
            String space_val = "";
            if (!digit_eng.equals("")) space_dig = " ";
            if (!value.equals("")) space_val = " ";
            number_in_english += digit_eng + space_dig + value + space_val;
        }

        System.out.println(number_in_english);
    }
}
