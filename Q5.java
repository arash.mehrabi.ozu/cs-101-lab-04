import java.util.Scanner;

public class Q5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        int max_num = -1;
        while (true) {
            num = scanner.nextInt();
            if(num < 0) {
                break;
            }

            if (max_num < num) {
                max_num = num;
            }
        }

        System.out.println("The maximum number of the numbers that you entered is: " + max_num);
    }
}
