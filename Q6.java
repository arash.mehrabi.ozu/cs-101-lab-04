import java.util.Scanner;

public class Q6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        boolean is_prime = true;
        for(int i=2; i < num/2 + 1; i++) {
            if(num % i == 0) {
                is_prime = false;
                break;
            }
        }

        if (is_prime) {
            System.out.println("The number " + num + " is prime.");
        } else {
            System.out.println("The number " + num + " is not prime.");
        }
    }
}
