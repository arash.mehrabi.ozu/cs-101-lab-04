import java.util.Scanner;

public class Q1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int digits_counter = 0;

        String digits = "";
        while(num > 0) {
            digits = digits + num%10 + ", ";
            digits_counter++;
            num /= 10;
        }

        if(digits_counter == 1) {
            System.out.print("The number has 1 digit. The digit is: " + digits);
        } else {
            System.out.println("The number has " + digits_counter + " digits. The digits are: " + digits);
        }
    }
}
