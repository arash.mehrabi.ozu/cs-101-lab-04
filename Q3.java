import java.util.Scanner;

// Java Program to convert decimal number to
// roman numerals
class Q3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        // If number entered is not valid
        if (number <= 0) {
            System.out.printf("Invalid number");
            return;
        }

        String roman = "";
        // TO convert decimal number to roman numerals
        while (number != 0) {
            // If base value of number is greater than 1000
            if (number >= 1000) {
                // Add 'M' number/1000 times
                int n = number / 1000;
                for(int j=0; j<n; j++) {
                    roman += "M";
                }
                number = number % 1000;
            }
            // If base value of number is greater than or
            // equal to 500
            else if (number >= 500) {
                if (number < 900) {
                    // Add 'D' number/1000 times
                    int n = number / 500;
                    for(int j=0; j<n; j++) {
                        roman += "D";
                    }
                    number = number % 500;
                } // To handle subtractive notation in case of number
                // having digit as 9 and adding corresponding base
                // symbol
                else {
                    // Add C and M.
                    roman += "CM";
                    number = number % 100;
                }
            } // If base value of number is greater than or equal to 100
            else if (number >= 100) {
                if (number < 400) {
                    int n = number / 100;
                    for(int j=0; j<n; j++) {
                        roman += "C";
                    }
                    number = number % 100;
                } // To handle subtractive notation in case of number
                // having digit as 4 and adding corresponding base
                // symbol
                else {
                    roman += "CD";
                    number = number % 100;
                }
            } // If base value of number is greater than or equal to 50
            else if (number >= 50) {
                // To add base symbol to the character array
                if (number < 90) {
                    int n = number / 50;
                    for(int j=0; j<n; j++) {
                        roman += "L";
                    }
                    number = number % 50;
                } // To handle subtractive notation in case of number
                // having digit as 9 and adding corresponding base
                // symbol
                else {
                    roman += "XC";
                    number = number % 10;
                }
            } // If base value of number is greater than or equal to 10
            else if (number >= 10) {
                // To add base symbol to the character array
                if (number < 40) {
                    int n = number / 10;
                    for(int j=0; j<n; j++) {
                        roman += "X";
                    }
                    number = number % 10;
                } // To handle subtractive notation in case of
                // number having digit as 4 and adding
                // corresponding base symbol
                else {
                    roman += "XL";
                    number = number % 10;
                }
            } // If base value of number is greater than or equal to 5
            else if (number >= 5) {
                if (number < 9) {
                    int n = number / 5;
                    for(int j=0; j<n; j++) {
                        roman += "V";
                    }
                    number = number % 5;
                } // To handle subtractive notation in case of number
                // having digit as 9 and adding corresponding base
                // symbol
                else {
                    roman += "IX";
                    number = 0;
                }
            } // If base value of number is greater than or equal to 1
            else if (number >= 1) {
                if (number < 4) {
                    int n = number / 1;
                    for(int j=0; j<n; j++) {
                        roman += "I";
                    }
                    number = 0;
                } // To handle subtractive notation in case of
                // number having digit as 4 and adding corresponding
                // base symbol
                else {
                    roman += "IV";
                    number = 0;
                }
            }
        }

        // Printing equivalent Roman Numeral
        System.out.printf("Roman numeral is: " + roman);
    }
}
// This code is contributed by PrinciRaj1992 on Geeks4Geeks
// Modified by Arash Mehrabi

