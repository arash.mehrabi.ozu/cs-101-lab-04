import java.util.Scanner;

public class Q4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double r = 0;
        double h = 0;
        while (true) {
            r = scanner.nextDouble();
            h = scanner.nextDouble();

            if(r > 0 && h > 0) {
                break;
            }
        }

        double v = (Math.PI * Math.pow(r, 2) * h) / 3;
        System.out.println("The volume of the cone is: " + v);
    }
}
