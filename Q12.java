import java.util.Scanner;

public class Q12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n_1 = scanner.nextInt();
        int n_2 = scanner.nextInt();
        int num = 0;

        int counter = 2;
        while (true) {
            num = scanner.nextInt();

            if (num == n_1 + n_2) {
                break;
            }

            n_2 = n_1;
            n_1 = num;
            counter++;
        }

        System.out.println("The sum is " + num + ", and the number of integers in the sequence except the last one is " + counter);

    }
}