import java.util.Scanner;

public class Q7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double sum_grades = 0;
        int num_grades = 0;

        while (true) {
            System.out.println("Please enter the grade: (Enter negative number when there are no more grades left)");
            double grade = scanner.nextDouble();
            if(grade < 0) break;

            sum_grades += grade;
            num_grades++;
        }

        System.out.println("The average of grades is: " + sum_grades / num_grades);
    }
}
