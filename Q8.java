import java.util.Scanner;

public class Q8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int sum_expression = 0;

        for (int i=1; i<=N; i++) {
            sum_expression += Math.pow(i, 2);
        }

        System.out.println("The sum of the expression is: " + sum_expression);
    }
}
