import java.util.Scanner;

public class Q9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int y = scanner.nextInt();

        int result = 1;
        for(int i=0; i<y; i++) {
            result = result * x;
        }

        System.out.println(x + " ^ " + y + " = " + result);
    }
}
