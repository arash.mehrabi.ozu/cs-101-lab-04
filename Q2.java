import java.util.Scanner;

public class Q2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        int sum = 0;
        while(num > 0) {
            sum += num %10;
            num /= 10;
        }
        System.out.println("The sum of the digits in the integer is: " + sum);
    }
}
