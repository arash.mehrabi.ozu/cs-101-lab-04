import java.util.Scanner;

public class Q13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double W = scanner.nextDouble();
        int N = scanner.nextInt();

        for (int i=0; i<N; i++){
            W = W - (W/10);
        }
        System.out.println("The weight after " + N + " weeks is " + (int) W);

    }
}
