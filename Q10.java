import java.util.Scanner;

public class Q10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int counter = 0;

        while (true) {
            int num = scanner.nextInt();
            if (num < 0) break;

            if (num % x == y) {
                counter += 1;
            }
        }

        String answer = "The number of integers in the sequence that gives a remainder " + y + " when divided by ";
        answer += x + " is " + counter + " .";
        System.out.println(answer);
    }
}
